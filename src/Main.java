public class Main {

//    Napisz program wyznaczający najmniejszą wspólną wielokrotność (NWW) dla pary liczb całkowitych dodatnich
//    hint:
//    Obliczaj wielokrotności jednej liczby i sprawdzaj, czy obliczona wielokrotność jest wielokrotnością drugiej liczby
// (czy dzieli się bez reszty przez drugą liczbę)

    public static void main(String[] args) {
        System.out.println(NWW(24,36));
    }
    public static double NWW(int a, int b){
        int pom;
        int a1=a, b1=b;

        while(b!=0)
        {
            pom = b;
            b = a%b;
            a = pom;
        }
        return a1/a*b1;
    }
}
